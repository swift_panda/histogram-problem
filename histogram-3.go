package main

import (
	"fmt"
	"math"
)

func findWidth(stack []int, i int) (width int) {
	if len(stack) == 0 {
		width = i
	} else {
		width = i - stack[len(stack)-1] - 1
	}
	return
}

func maxHistogramArea(hist []int) int {
	var area float64
	stack := make([]int, 0)
	i := 0
	for i < len(hist) {
		if len(stack) == 0 || (hist[stack[len(stack)-1]] <= hist[i]) {
			stack = append(stack, i)
			i++
		} else {
			var top int
			top, stack = stack[len(stack)-1], stack[:len(stack)-1]
			width := findWidth(stack, i)
			area = math.Max(area, float64(width*hist[top]))
		}
	}
	for len(stack) > 0 {
		var top int
		top, stack = stack[len(stack)-1], stack[:len(stack)-1]
		width := findWidth(stack, i)
		area = math.Max(area, float64(width*hist[top]))
	}
	return int(area)
}

func main() {
	histA := []int{1, 3, 2, 0, 4, 7, 2}
	fmt.Println(maxHistogramArea(histA))
}
