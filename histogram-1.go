package main

import (
	"fmt"
	"math"
)

// O(n^2) solution
func maxRectangularArea(hist []int) int {
	var area float64
	for i := range hist {
		minHeight := float64(hist[i])
		for x := i; x < len(hist); x++ {
			minHeight = math.Min(minHeight, float64(hist[x]))
			area = math.Max(area, float64(x-(i-1))*minHeight)
		}
	}
	return int(area)
}

func main() {
	histA := []int{1, 3, 2, 0, 4, 7, 2}
	fmt.Println(maxRectangularArea(histA))
}
